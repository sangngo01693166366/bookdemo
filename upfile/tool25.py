from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import QFile, QObject
from functools import partial
import os
import pymxs
import sys
import shutil

rt = pymxs.runtime

from PolyOb import Poly
#include MaxPlus
#Luon return ra 2 dictionary, 1 error dictionary va 1 non-error dictionary

functionDescription = "Check NGons"

ErrMessage = {
	"error":" dang bi loi NGons",
	"ok":" khong bi loi NGons",
	"suggest": " hay convert sang EditPoly va chay lai function de check"
}

pymxs.runtime.execute("function b2a b = (return b as Array)")
ErrorList= {}

def checkNGons():
	errorDict = {}
	nonErrorDict = {}
	pymxs.runtime.execute("global checkSelectionVar = True")
	pymxs.runtime.execute("if $ == undefined do checkSelectionVar = False")
	if rt.checkSelectionVar == False:
		for obj in rt.geometry:
			try:
				NGons = Poly.NGonFaces(obj)
				print (NGons)
				if len(NGons):
					ErrorList[obj.name] = NGons
					errorDict[obj.name] = ErrMessage["error"]
					print (ErrorList)
				else:
					nonErrorDict[obj.name] = ErrMessage["ok"]
			except RuntimeError as err:
				print (err)
				errorDict[obj.name] = ErrMessage["suggest"]	
		return errorDict, nonErrorDict
	else:
		for obj in rt.selection:
			try:
				NGons = Poly.NGonFaces(obj)
				if len(NGons):
					ErrorList[obj.name] = NGons
					errorDict[obj.name] = ErrMessage["error"]
				else:
					nonErrorDict[obj.name] = ErrMessage["ok"]
			except RuntimeError:
				errorDict[obj.name] = ErrMessage["suggest"]
		return errorDict, nonErrorDict

def selectCustom(*args):
	for asset in args[0]:
		obj = rt.getnodebyname(asset)
		try:
			rt.select(obj)
			rt.polyop.setFaceSelection(obj,ErrorList[obj.name])
			pymxs.runtime.execute("subobjectlevel = 4")
		except RuntimeError:
			pass


def result():
	errorDict, nonErrorDict = checkNGons()
	return [errorDict, nonErrorDict]