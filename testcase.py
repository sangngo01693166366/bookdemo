# Run this script in 3ds Max Batch
import sys
import ast
import os
from modulefinder import ModuleFinder

def test_source_code_compatible(code_data):
    try:
        return ast.parse(code_data)
    except SyntaxError as exc:
        print(exc)
        return False

# sys.argv[1] ==> name of file txt

f = open("result/result.txt", "r")

for line in f:
    if(line == ".gitlab-ci.yml\n"):
        continue 
    line = str(line).strip()
    print(line)

    ast_tree = test_source_code_compatible(open(line, 'rb').read())

    if not ast_tree:
        print("Python 2")
        os.system("echo 's' >> result2/python2.txt")
        break
    else:
        with open(line, "r") as f:
            #data = f.read()

            # Get modules loaded in file
            finder = ModuleFinder()
            finder.run_script(line)
            
            #MaxCases contain Max <= 2020 modules that doesn't appear in Max 2022
            with open("MaxCases.txt") as MaxCases:
                cases = MaxCases.readlines()
            cases = [x.strip() for x in cases]

            # module name matching with MaxCases
            if any(name in cases for name in finder.modules.keys()):
                print("Python 2")
                os.system("echo 's' >> result2/python2.txt")
                break

        print("Python 3")
        os.system("echo 's' >> result2/python2.txt")
        break 

f.close()
