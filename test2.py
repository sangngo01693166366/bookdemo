import ast

def test_source_code_compatible(code_data):
    try:
        return ast.parse(code_data)
    except SyntaxError as exc:
        return False

ast_tree = test_source_code_compatible(open('upfile.py', 'rb').read())
if not ast_tree:
    print("File couldn't get loaded")
else:
    print("File is loaded")
    
print("TEST2.py DONE TEST RUN")